const app = require('./app')

const request = require("supertest")

describe("GET /", () => {
    it("should return 200", async () => {
        const response = await request(app).get("/");
        expect(response.statusCode).toBe(200);
    });
});